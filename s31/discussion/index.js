// use the require directive to load node.js
// A package or module is a software component or part of a program that contains one or more routine
//http module lets node js transfer data using Hyper Text TransFer Protocol
//Clients (browser) and servers (node/express) communicate by exchanging individual messages


let http = require("http");
//Using this module "createServer()" method we can create an http server that listens on a specified port.

// A port is a virtual point where network  connection start and end
//each port is associated with specific process or servers
http.createServer(function (request, response) {

	//writeHead() method
	//set the status code for the response- 200 -> ok/success

	response.writeHead(200, {"Content-Type" : "text/plain"});

	//send the response with text content "Hello  World"
	response.end("Hello World!");

}).listen(4000);

//whenever the server starts , console will print the message in our "terminal"
console.log("server running at localhost:4000");
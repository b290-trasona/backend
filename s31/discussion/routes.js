const http = require("http");

//creates variable "port" to store the port number
const port = 4001;

//creates a variable "app"that stores the output of the createServer() method.
const app = http.createServer((req, res) => {


	if(req.url == "/greeting"){
		res.writeHead(200, {"Content-Type" : "text/plain"});

		//send the response with text content "Hello  World"
		res.end("Hello Again!");
	} else if (req.url == "/homepage") {
		res.writeHead(200, {"Content-Type" : "text/plain"});

		//send the response with text content "Hello  World"
		res.end("This is the homepage");
	} else {
		res.writeHead(404, {"Content-Type" : "text/plain"});

		//send the response with text content "Hello  World"
		res.end("404: Page not found");
	}

});

//Uses the "app" and "port" variables created above

app.listen(port);

console.log(`Server now running at localhost:${port}.`);

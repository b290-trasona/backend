//console.log("hello")

/*function printInput(){
	let nickName = prompt("Enter ur nickname");
	return "hi"+nickName;
};

console.log(printInput());*/

/*You can directly pass data into the function. The function can then call/use that data which is referred as "name" within the function.

"name" is called a parameter.

A "parameter" acts as a named variable/container that exists only inside of a function
It is used to store information that is provided to a function when it is called/invoked.
*/
//
// parameters with the prompt don't need hardcoded arguments since the values will be provided via prompt


function printName(name){
	return "My name is "+name;
};
console.log(printName("janie"));

let anotherName = "Mitsuha";
//variables can be passed as arguments
console.log(printName(anotherName));

function printInput(nickname){
	nickname = prompt("enter nickname");
	return "HI " +nickname;
};
/*
the information/data provided directly into the function, is called an argument. Values passed when invoking a function are called arguments. These arguments are then stored as the parameters within the function.*/

//console.log(printInput());


function checkDivisibilityBy8(num){
	let remainder = num % 8;
	console.log("the remainder of "+num+"divided by 8 is "+remainder);

	let isDivisibleBy8 = remainder === 0 ;
	console.log("is"+num+"divisible by 8?");
	console.log(isDivisibleBy8);
};

checkDivisibilityBy8(64);
checkDivisibilityBy8(28);


//functions as arguments
/*Function parameters can also accept other functions as arguments
Some complex functions use other functions as arguments to perform more complicated results.
This will be further seen when we discuss array methods.*/

function argumentFunction(){
	console.log("This function was passed as an argument before the message was printed");
};

function invokeFunction(argumentFunction){
	argumentFunction();
};

invokeFunction(argumentFunction);
console.log(argumentFunction);

//using multiple parameters
/*Multiple "arguments" will correspond to the number of "parameters" declared in a function in succeeding order.*/

function createFullName(firstName, middleName, lastName){
	return firstName+" "+middleName+" "+lastName
};
console.log(createFullName("Juan","dela","Cruz","reyes"));
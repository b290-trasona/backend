const express = require("express");
const mongoose = require("mongoose");
// Mongoose is a package that allows creation of Schemas to model our data structures
	// Also has access to a number of methods for manipulating our database
const app = express();
const port = 3001;

// [Section] MongoDB connection

	// Connect to the database by passing in your connection string, remember to replace the password and database names with actual values
	// Due to updates in Mongo DB drivers that allow connection to it, the default connection string is being flagged as an error
	// By default a warning will be displayed in the terminal when the application is run, but this will not prevent Mongoose from being used in the application
	// { newUrlParser : true } allows us to avoid any current and future errors while connecting to Mongo DB

	// Syntax
		// mongoose.connect("<MongoDB Atlas connection string>", { useNewUrlParser : true });

mongoose.connect("mongodb+srv://admin:admin123@zuitt.mbcnz2k.mongodb.net/b290-to-do?retryWrites=true&w=majority",
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}

);

// Connecting to MongoDB locally
	// 27017 is the default port on where mongo instances are run in a device
	// mongoose.connect("mongodb://localhost:27017/bXX-to-do", 
	// 	{ 
	// 		useNewUrlParser : true,  
	// 		useUnifiedTopology : true
	// 	}
	// );


// Set notifications for connection success or failure
	// Connection to the database
	// Allows to handle errors when the initial connection is establised
	// Works with the on and once Mongoose methods
let db = mongoose.connection;

// If a connection error occurred, output in the console
// console.error.bind(console) allows us to print errors in the browser console and in the terminal
// "connection error" is the message that will display if an error is encountered
db.on("error", console.error.bind(console,"db connection error"));

// If the connection is successful, output in the console
db.once("open", () => console.log("we're connected in the cloud db"));

 // [Section] Mongoose Schemas

	// Schemas determine the structure of the documents to be written in the database
	// Schemas act as blueprints to our data
	// Use the Schema() constructor of the Mongoose module to create a new Schema object
	// The "new" keyword creates a new Schema

const taskSchema = new mongoose.Schema({

	// Define the fields with the corresponding data type
	// For a task, it needs a "task name" and "task status"
	// There is a field called "name" and its data type is "String"
	name : String,

	// There is a field called "status" that is a "String" and the default value is "pending"
	status : {
		type : String,

		// Default values are the predefined values for a field if we don't put any value
		default : "pending"
	}
});


// [Section] Models
	// Uses schemas and are used to create/instantiate objects that correspond to the schema
	// Models use Schemas and they act as the middleman from the server (JS code) to our database
	// Server > Schema (blueprint) > Database > Collection

	// The variable/object "Task" and "User" can now used to run commands for interacting with our database
	// "Task" and "User" are both capitalized following the MVC approach for naming conventions
	// Models must be in singular form and capitalized
	// The first parameter of the Mongoose model method indicates the collection in where to store the data
	// The second parameter is used to specify the Schema/blueprint of the documents that will be stored in the MongoDB collection
	// Using Mongoose, the package was programmed well enough that it automatically converts the singular form of the model name into a plural form when creating a collection in postman
    
const Task = mongoose.model("Task", taskSchema);

//ACTIVITY  USER SCHEMA

const userSchema = new mongoose.Schema({
	username : String,
	password : String
});

//ACTIVITY USER MODEL

const User = mongoose.model("User", userSchema);


//MIDDLEWARE
// Setup for allowing the server to handle data from requests
//allows our app to read json data
app.use(express.json());
app.use(express.urlencoded({extended : true}));

//SEction ROutes
// creating a new task
// Business Logic
	/*
	1. Add a functionality to check if there are duplicate tasks
		- If the task already exists in the database, we return an error
		- If the task doesn't exist in the database, we add it in the database
	2. The task data will be coming from the request's body
	3. Create a new Task object with a "name" field/property
	4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
	*/

app.post("/newTask", (req,res) => {

	// Check if there are duplicate tasks
	// "findOne" is a Mongoose method that acts similar to "find" of MongoDB
	// findOne() returns the first document that matches the search criteria as a single object.
	// findOne() can send the possible result or error in another method called then() for further processing.
	// .then() is chained to another method/promise that is able to return a value or an error.
	// .then() waits for the previous method to complete its process. It will only run once the previous method is able to return a value or error.
	// .then() method can then process the returned result or error in a callback method inside it.
	// the callback method in the then() will be able to receive the result or error returned by the previous method it is attached to.
	// the .findOne() method returns the result first and the error second as parameters. 
	    
	    

	Task.findOne({name: req.body.name}).then((result)=>{

		console.log(result);

		// If a document was found and the document's name matches the information sent via the client/Postman
		if (result != null && result.name == req.body.name){

			// Return a message to the client/Postman
			return res.send("dulicate task found");

			// If no document was found
		} else {

			// Create a new task and save it to the database
			let newTask = new Task({
				name : req.body.name
			});

			// The "save" method will store the information to the database
			// Since the "newTask" was created/instantiated from the Mongoose Schema it will gain access to this method to save to the database
			// The "save()" method can send the result or error in another JS method called then()
			// the .save() method returns the result first and the error second as parameters.

			newTask.save().then((savedTask, savedErr)=> {

				//If there arae errors in saving
				if(savedErr){

					// Will print any errors found in the console
					// saveErr is an error object that will contain details about the error
					// Errors normally come as an object data type
					return console.log(savedErr);

					// No error found while creating the document
				} else {

					// Return a status code of 201 for created
					// Sends a message "New task created" on successful creation
					return res.status(201).send("new task created");
				}
			})
		}
	}).catch(err => res.send(err))
});

// Getting all the tasks

// Business Logic
	/*
	1. Retrieve all the documents
	2. If an error is encountered, print the error
	3. If no errors are found, send a success status back to the client/Postman and return an array of documents
	*/
    
app.get("/tasks",(req,res)=>{


	/*Task.find({}).then(result =>{
		console.log(result);

		return res.send(result)


	}).catch(err => res.send(err))*/

	// "find" is a Mongoose method that is similar to Mongodb "find", and an empty "{}" means it returns all the documents and stores them in the "result" parameter of the callback function
	Task.find({}).then((result,err)=> {
		if(err){
			return console.log(err);
		} else {

			// Status "200" means that everything is "OK" in terms of processing
							// The "json" method allows to send a JSON format for the response
							// The returned response is purposefully returned as an object with the "data" property to mirror real world complex data structures
			return res.status(200).json({
				date: result
			})
		}
	})
});


//ACTIVITY ROUTES
app.post("/signup",(req,res)=> {

	//console.log(typeof req.body.password);

	User.findOne({username : req.body.username})
		.then((result)=>{

			if(result !== null && result.username == req.body.username){
				return res.send("Duplicate username found");
			} else {


				//damay na lahat
				if(req.body.username !== "" && req.body.username !== null && req.body.username !== undefined && req.body.password !== null && req.body.password !== undefined && req.body.password !== "" ){
					let newUser = new User({
						username : req.body.username,
						password : req.body.password
					});

					newUser.save()
						.then((savedUser,savedErr)=> {

							if(savedErr){
								return console.log(savedErr);
							} else {
								return res.status(201).send("New user registered")
							}
						})
				} else {
					return res.send("BOTH username and password must be provided")
				}
			}

		})

});











if(require.main === module){
	app.listen(port, () => console.log(`server running at pot ${port}`))
}

module.exports = app;
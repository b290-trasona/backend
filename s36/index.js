//set up dependencies
const express = require("express");
const mongoose = require("mongoose");

const taskRoute = require("./routes/taskRoute.js");

//server setup
const app = express()
const port = 4000;

//Database connection

mongoose.connect("mongodb+srv://admin:admin123@zuitt.mbcnz2k.mongodb.net/b290-to-do?retryWrites=true&w=majority",

	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}


);

let db = mongoose.connection;

db.on("error",console.error.bind(console,"connection error"));
db.once("open", ()=> console.log("connected to mongodb"));

//setup middleware
app.use(express.json());
app.use(express.urlencoded({extended : true}));



//setup routes
app.use("/tasks", taskRoute);








if(require.main === module){
	app.listen(port, ()=> console.log(`server running at ${port}`))
}

module.exports = app;
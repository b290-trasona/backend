const express = require("express");
const mongoose = require("mongoose");
// Allows our backend application to be available to our frontend application
// Allows us to control the app's Cross Origin Resource Sharing settings
const cors = require("cors");

// Allows access to routes defined within our applications
const userRoute = require("./routes/userRoute.js");
const courseRoute = require("./routes/courseRoute.js");

const app = express();

//Connect mongodb database

mongoose.connect("mongodb+srv://admin:admin123@zuitt.mbcnz2k.mongodb.net/booking-DB?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

mongoose.connection.once("open", () => console.log("connected to mongodb Atlas"));

//middlewares
// Allows all resources to access our backend application
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));


//Router
// Defines the "/users" string to be included for all user routes defined in the "user" route file
app.use("/users", userRoute);

// Defines the "/courses" string to be included for all course routes defined in the "course" route file
app.use("/courses", courseRoute);








if(require.main === module){
	app.listen(process.env.PORT || 4000, ()=> {
		console.log(`API is now online on port ${process.env.PORT || 4000}`)
	})
};

module.exports = {app,mongoose};
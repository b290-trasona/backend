const express = require("express");
const router = express.Router();

const auth = require("../auth.js");

const courseController = require("../controllers/courseController.js");

router.post("/", auth.verify, (req,res) =>{

	let userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin !== true ){
		console.log("user is not admin")
		return  res.send(false);
	} else {

		courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
	}

	
});

router.get("/all", auth.verify, (req,res)=> {
	let userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin !== true ){
		console.log("user is not admin")
		return  res.send(false);
	} else {

		courseController.getAllCourses().then(resultFromController => res.send(resultFromController))
	}

	
});

//route for getting all courses
router.get("/",(req,res) => {

	courseController.getAllActiveCourses().then(resultFromController => res.send(resultFromController));
})

// Route for retrieving a specific course
		// Creating a route using the "/:parameterName" creates a dynamic route, meaning the url is not static and changes depending on the information provided in the url
router.get("/:courseId", (req,res) => {
	console.log(req.params);


	// Since the course ID will be sent via the URL, we cannot retrieve it from the request body
			// We can however retrieve the course ID by accessing the request's "params" property which contains all the parameters provided via the url
				// Example: URL - http://localhost:4000/courses/613e926a82198824c8c4ce0e
				// The course Id is "613e926a82198824c8c4ce0e" which is passed via the url that corresponds to the "courseId" in the route
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController))



});

// Route for updating a course
		// JWT verification is needed for this route to ensure that a user is logged in before updating a course
router.put("/:courseId", auth.verify,(req,res) => {

	const isAdmin = auth.decode(req.headers.authorization).isAdmin;
	if(isAdmin){

		courseController.updateCourse(req.params,req.body).then(resultFromController => res.send(resultFromController))
	} else {
		console.log("not Admin")
		return res.send(false)
	}
	
});

//archive course route
router.patch("/:courseId/archive", auth.verify,(req,res) => {

	let userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin !== true ){
		console.log("user is not admin")
		return  res.send(false);
	} else {

		courseController.archiveCourse(req.params.courseId,req.body).then(resultFromController => res.send(resultFromController));
	}

});





module.exports = router;
// S28 - MongoDB CRUD Operations Activity Template:

/*

    Sample solution:

    async function addOneQuery(db) {
        await (

            //add query here

            db.collectionName.insertOne({
                field1: "value1",
                field2: "value2"
            })
        
        );
        
        return(db);
    }

Note: 
    - Do note change the functionName or modify the exports


*/

// 1. Insert a single room (insertOne method) in the rooms collection:

async function addOneFunc(db) {

    await (

        // Add query here
        db.rooms.insertOne({
            name: "single",
            accomodates: 2,
            price: 1000,
            description: "A small room with all the basic necessities",
            roomsAvailable: 10,
            isAvailable: false
        });

    );


   return(db);

};


// 2. Insert multiple rooms (insertMany method)  in the rooms collection

async function addManyFunc(db) {

    await (

        // Add query here
        db.rooms.insertMany([
                {
                    name: "double",
                    accomodates: 3,
                    price: 2000,
                    description: "A room fit for a family going on a vacation",
                    roomsAvailable: 5,
                    isAvailable: false
                },
                {
                    name: "queen",
                    accomodates: 4,
                    price: 4000,
                    description: "A room with a queen size bed perfect for a simple getaway",
                    roomsAvailable: 15,
                    isAvailable: false
                }


        ]);

    );

   return(db);

};

// 3. Use the findOne method to search for a room with the name double.
async function findRoom(db) {
    return await (

        // Add query here
        db.rooms.findOne({name:"double"});

    );
};

// 4. Use the updateOne method to update the queen room and set the available rooms to 0.

function updateOneFunc(db) {

   // Add query here
    db.rooms.updateOne(
            {name: "queen"},
            {
                $set:{roomsAvailable: 0}
            }


    );

};


// 5. Use the deleteMany method to delete all rooms that have 0 rooms available.
function deleteManyFunc(db) {

   // Add query here
    db.rooms.deleteMany({
        roomsAvailable: 0
    });

};



try{
    module.exports = {
        addOneFunc,
        addManyFunc,
        updateOneFunc,
        deleteManyFunc,
    };
} catch(err){

};


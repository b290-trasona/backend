// CRUD OPertaions


//Inserting documents(Create)


//Inserting one document
//syntax: db.collectionName.insertOne({Object})
db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "09123456789",
		email: "janedoe@mail.com"
	},
	courses: ["CSS","Javascript","HTML"],
	department: "none"
});

//Inserting multiple documents
//syntax: db.collectionName.insertMany([{ObjectA},{ObjectB}..])
db.users.insertMany([
	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact: {
			phone: "09123456789",
			email: "stephenhawking@gmail.com"
		},
		courses: ["Python","React","PHP"],
		department: "none"
	},
	{
		firstName: "Neil",
		lastName: "Armstrong",
		age: 82,
		contact: {
			phone: "09123456789",
			email: "neilarmstrong@gmail.com"
		},
		courses: ["React","Laravel","Saas"],
		department: "none"
	}


]);

//Finding documents (READ)
//======>maybe not true anymore====>if multiple documents match the criteria,only the first document that matches will be returned?
/*Syntax: db.collectionName.find()
			db.collectionName.findOne()
           db.collectionName.find(field: value)

*/
//Leaving the search criteria empty will retrieve all the documents
db.users.find();
db.users.find({firstName: "Stephen"});


//finding documents with multiple parameters

db.users.find({ lastName: "Armstrong", age: 82 });


//Updating documents (UPDATE)
//Updating a single document
//Syntax db.collectionName.updateOne({criteria}, {$set: {field:value}})
/*
	updateOne() will only update the first document that matches the search criteria

*/
db.users.insertOne({
	firstName: "Test",
	lastName: "test",
	age: 0,
	contact: {
		phone: "0000000",
		email: "test@gmail.com"
	},
	courses: [],
	department: "none"
});

db.users.updateOne(
	{firstName: "Test"},
	{
		$set: {
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "09123456789",
				email: "billgates@gmail.com"
			},
			courses: ["PHP","Laravel","HTML"],
			department: "Operations",
			status:"active"
		}
	}
	
);

db.users.find({firstName: "Bill"});

//Updating multiple documents
/*
	syntax :
		db.collectionName.updateMany({criterea},{$set :{field: value}})
*/
db.users.updateMany(
	{department: "none"},
	{
		$set:{department:"HR"}
	}


);

//Replace One
db.users.replaceOne(
	 {firstName:"Bill"},
	 {
	 	firstName: "Bill",
	 	lastName: "Gates",
	 	age: 65,
	 	contact: {
	 		phone: "09123456789",
	 		email: "billgates@gmail.com"
	 	},
	 	courses: ["PHP","Laravel","HTML"],
	 	department: "Operations"
	 	
	 }

)

//Deleting documents(Delete)

//Document to be deleted
db.users.insertOne({
	firstName: "test"
});

//deleting a single document
/*
	Syntax:
		db.collectionName.deleteOne({criteria})

*/

db.users.deleteOne({firstName:"test"});

//Deleting Many
/*
	Syntax:
		db.collectionName.deleteMany({criteria})
*/
db.users.deleteMany({firstName:"Bill"});

//Section for advanced queries
db.users.find({
	contact:{
		phone: "09123456789",
		email: "stephenhawking@gmail.com"
	}
});

db.users.find({
	"contact.email":"janedoe@mail.com"
});
//querying an array with exact elements
db.users.find({courses:["CSS","Javascript","HTML"]});
//querying an array without reagard sa order
db.users.find({courses: {$all:["React","Python"]}});

db.users.insertOne({
	namearr: [
			{
				namea: "juan"
			},
			{
				nameb: "tamad"
			}

		]
});

db.users.find({
	namearr:{
		namea: "juan"
	}
});
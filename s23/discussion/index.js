//console.log("yoh");

//Objects
/*
		    - An object is a data type that is used to represent real world objects
		    - It is a collection of related data and/or functionalities
		    - In JavaScript, most core JavaScript features like strings and arrays are objects (Strings are a collection of characters and arrays are a collection of data)
		    - Information stored in objects are represented in a "key:value" pair
		    - A "key" is also mostly referred to as a "property" of an object
		    - Different data types may be stored in an object's property creating complex data structures
		*/

		// Creating objects using object initializers/literal notation
		/*
		    - This creates/declares an object and also initializes/assigns it's properties upon creation
		    - A cellphone is an example of a real world object
		    - It has it's own properties such as name, color, weight, unit model and a lot of other things
		    - Syntax
		        let objectName = {
		            keyA: valueA,
		            keyB: valueB
		        }
		*/

//Initializers/literal notation
let cellphone = {
	name: "Nokia 3210",
	manufactureDate: 1999
};

console.log("result from creating objects using initializers/literal notation");
console.log(cellphone);
console.log(typeof(cellphone));

//creating objects constructor function
/*
		    - Creates a reusable function to create several objects that have the same data structure
		    - This is useful for creating multiple instances/copies of an object
		    - An instance is a concrete occurence of any object which emphasizes on the distinct/unique identity of it
		    - Syntax
		        function ObjectName(keyA, keyB) {
		            this.keyA = keyA;
		            this.keyB = keyB;
		        }
		*/


// This is an object
// The "this" keyword allows to assign a new object's properties by associating them with values received from a constructor function's parameters    
function Laptop(name,manufactureDate){
	this.name = name;
	this.manufactureDate = manufactureDate;
};

// This is a unique instance of the Laptop object
/*
- The "new" operator creates an instance of an object
- Objects and instances are often interchanged because object literals (let object = {}) and instances (let object = new object) are distinct/unique objects
*/

let laptop = new Laptop("lenovo",2008);
console.log("result from creating objects using object constructors");
console.log(laptop);

/*
	- It invokes/calls the "Laptop" function instead of creating a new object instance
	- Returns "undefined" without the "new" operator because the "Laptop" function does not have a return statement
	- if there is no keyword <new>
*/

let myLaptop = new Laptop("MacBook Air",2020);
console.log("result from creating objects using object constructors");
console.log(myLaptop);

//Creating empty Objects;
let computer = {};
let myComputer = new Object();
console.log(computer);
console.log(myComputer);


//Accessing Object Properties
//using the dot notation
console.log("Result of the dot notation"+myLaptop.name);

//Using the Square bracket notation
console.log("Result of the dot notation"+myLaptop["name"]);

//Accessing Array Objects
/*
		    - Accessing array elements can be also be done using square brackets
		    - Accessing object properties using the square bracket notation and array indexes can cause confusion
		    - By using the dot notation, this easily helps us differentiate accessing elements from arrays and properties from objects
		    - Object properties have names that makes it easier to associate pieces of information
		*/

let array =[Laptop,myLaptop];
// May cause confusion for accessing array indexes
console.log(array[0]["name"]);
// Differentiation between accessing arrays and object properties
		// This tells us that array[0] is an object by using the dot notation
console.log(array[0].name);


//Initializing/adding/deleting/Reassigning object properties
/*
		    - Like any other variable in JavaScript, objects may have their properties initialized/added after the object was created/declared
		    - This is useful for times when an object's properties are undetermined at the time of creating them
		*/    
let car = {};

car.name = "Honda Civic";
console.log("Result from adding properties using dot notation");
console.log(car);

// Initializing/adding object properties using bracket notation
		/*
		    - While using the square bracket will allow access to spaces when assigning property names to make it easier to read, this also makes it so that object properties can only be accesssed using the square bracket notation
		    - This also makes names of object properties to not follow commonly used naming conventions for them
		*/
car["manufacture date"]= 2019;
console.log(car["manufacture date"]);
console.log(car["Manufacture date"]);
console.log(car.manufactureDate);
console.log("Result from adding properties using square bracket notation");
console.log(car);

//Deleting Object Properties
delete car["manufacture date"];
console.log("REsult form deleting :");
console.log(car);

//Reassigning Object Properties
car.name = "Volkswagen Beetle";
console.log("Result from reassigning properties");
console.log(car);

// let person = {};
// person.name ="John";
// person.age = 50;
// person.personality = "Polite"

function Person(name,age,personality,deviceUnit){
	this.name = name;
	this.age = age;
	this.personality = personality;
	this.deviceUnit = deviceUnit;
};

let myFriend1 = new Person("John",50,"Polite","Samsung");
let myFriend2 = new Person("Mark",70,"Courteous","ASUS");
console.log(myFriend1);
console.log(myFriend2);

//Object Methods
/*
		    - A method is a function which is a property of an object
		    - They are also functions and one of the key differences they have is that methods are functions related to a specific object
		    - Methods are useful for creating object specific functions which are used to perform tasks on them
		    - Similar to functions/features of real world objects, methods are defined based on what an object is capable of doing and how it should work
		*/    

let person = {
	name: "Juan",
	talk: function(){
		console.log("Kumusta ako si "+this.name)
	}
}

console.log(person);
console.log("result for object methods");
person.talk();

person.walk = function(){
	console.log(this.name + " walked 25 steps forward");
}
person.walk();

let friend = {
	firstName: "Pedro",
	lastName: "Penduko",
	address: {
		city: "Manila",
		country: "Philippines"
	},
	emails: ["pedro@mail.com", "penduko@mail.com"],
	introduce: function(){
		console.log("Hello My name is "+this.firstName+" "+this.lastName);
	}
}

friend.introduce();

//Real world Application of objects
  /*
		    - Scenario
		        1. We would like to create a game that would have several pokemon interact with each other
		        2. Every pokemon would have the same set of stats, properties and functions
		*/
let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log(name+" tackled targetPokemon");
		console.log("target pokemons health is now _targetPokemonHealth_");

	},
	faint: function(){
		console.log("Pokemon fainted")
	}
}

console.log(myPokemon);

// Creating an object constructor instead will help with this process

function Pokemon(name,level){
	this.name =name;
	this.level = level;
	this.health = 2*level;
	this.attack = level;

	//methods
	this.tackle = function(target){
		console.log(this.name+" tackled "+target.name)
		console.log(target.name+" heath is now reduced to targetPokemonsHealth");
	},
	this.faint = function(){
		console.log(this.name +" fainted");
	}
};
// Creates new instances of the "Pokemon" object each with their unique properties

let pikachu = new Pokemon("Pikachu",16);
let rattata = new Pokemon("Ratata",8);

// Providing the "rattata" object as an argument to the "pikachu" tackle method will create interaction between the two objects
pikachu.tackle(rattata);
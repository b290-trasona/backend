//console.log("hello")

// functions
// Functions in javascript are lines/blocks of codes that tell our device/application to perform a certain task when called/invoked
	// Functions are mostly created to create complicated tasks to run several lines of code in succession
	// They are also used to prevent repeating lines/blocks of codes that perform the same task/function

	//function declaration
	//(function statement) defines a function with the specified parameters.
	/*
		function functionName(){
			statement;
			statement;
			statement;
		};
	*/
	// function keyword - used to defined a javascript functions
	// functionName - the function name. Functions are named to be able to use later in the code.
	// function block ({}) - the statements which comprise the body of the function. This is where the code to be executed.
	
	function printName(){
		console.log("My name is Janie");
	};

// function invocation
//The code block and statements inside a function is not immediately executed when the function is defined. The code block and statements inside a function is executed when the function is invoked or called.
	//It is common to use the term "call a function" instead of "invoke a function".

	printName();

	//printAge(); functions should be declared

//fucntion declarations vs expressiona
	//Function declaration
	//A function can be created through function declaration by using the function keyword and adding a function name.

		//Declared functions are not executed immediately. They are "saved for later use", and will be executed later, when they are invoked (called upon).

	declaredFunction(); // declared functions can be hoisted as long as its defined

	//Note: Hoisting is Javascript's behavior for certain variables and functions to run or use them before their declaration

	function declaredFunction(){
		console.log("hellollll");
	};

	//function expression
		//A function can also be stored in a variable. This is called a function expression.

		//A function expression is an anonymous function assigned to the variableFunction

		//Anonymous function function without a name
		//variableFunction();

		let variableFunction = function(){
			console.log("yeahyeah");
		};

		variableFunction();

		//A function expression of a function named func_name assigned to the variable funcExpression
		//How do we invoke the function expression?
		//They are always invoked (called) using the variable name.

		let funcExpression = function funcName() {
			console.log("to tell you im sorry");
		};

		funcExpression();

		//You can reassign declared functions and function expressions to new anonymous functions.
		declaredFunction = function(){
			console.log("updated declaredFunction");
		};
		declaredFunction();

		funcExpression = function(){
			console.log("updated funcExpression");
		};
		funcExpression();


		const constantFunc = function(){
			console.log("initialied with const");
		};
		constantFunc();

		//constant assignment error
		/*constantFunc = function(){
			console.log("try lang");
		};

		constantFunc();*/

//function scoping
/*	
	Scope is the accessibility (visibility) of variables.
	
	Javascript Variables has 3 types of scope:
		1. local/block scope
		2. global scope
		3. function scope
			JavaScript has function scope: Each function creates a new scope.
			Variables defined inside a function are not accessible (visible) from outside the function.
			Variables declared with var, let and const are quite similar when declared inside a function
*/

	{
		let localVar = "Armando Perez"
	}

	let globalVar = "Mr. Wordwide";

	console.log(globalVar);
	//console.log(localVar);//error not localvar not definec
	
	function showNames(){
		//function scoped variables
		var functionVar = "Arthas";
		const functionConst = "Porthas";
		let functionLet = "Aramis";
		console.log(functionVar);
		console.log(functionConst);
		console.log(functionLet);
	}

	showNames();

	// All results to an error as NOt defined
	//console.log(functionVar);
	//	console.log(functionConst);
	//	console.log(functionLet);

	//Nested functions
		//another function inside a function

	function myNewFunction(){
		let name = "jane";
		function nestedfunction(){
			let nestedName = "john";
			console.log(name);
		};
		//console.log(nestedName);//error nestedName is not defined
    //at myNewFunction 
		nestedfunction();
	};

	myNewFunction();
	//nestedfunction();

	//function and global scoped variables
		//global scoped variable

	let globalName = "Bill GAts";

	function myNewFunction2(){
		let nameInside = "Steve Jobs";

		console.log(globalName);
	}

	myNewFunction2();
	//console.log(nameInside);//nameInside is not defined
    //at index

//Using Alerts
//alert() allows us to show a small window at the top of our browser page to show information to our users. As opposed to a console.log() which only shows the message on the console. It allows us to show a short dialog or instruction to our user. The page will wait until the user dismisses the dialog.

	//alert("hello");

	function showSampleAlert(){
		alert("hello");
	};
	//showSampleAlert();

	console.log("Iwill only log in the console when the alert is dismissed");
	//Notes on the use of alert():
		//Show only an alert() for short dialogs/messages to the user. 
		//Do not overuse alert() because the program/js has to wait for it to be dismissed before continuing.

//using prompts 
	//prompt() allows us to show a small window at the of the browser to gather user input. It, much like alert(), will have the page wait until the user completes or enters their input. The input from the prompt() will be returned as a String once the user dismisses the window.

/*	let samplePrompt = prompt("enter name");

	console.log("Hello"+samplePrompt);
	console.log(typeof samplePrompt);

	//prompt("enter anything");*/
	//let sampleNullPrompt = prompt("Dont enter anything");
	//console.log(sampleNullPrompt);
	//prompt() returns an empty string when there is no input. Or null if the user cancels the prompt().
	//prompt() can be used to gather user input and be used in our code. However, since prompt() windows will have the page wait until the user dismisses the window it must not be overused.

	//prompt() used globally will be run immediately, so, for better user experience, it is much better to use them accordingly or add them in a function.

	function printWelcomMessage() {
		let firstName = prompt ("Enter your first name");
		let lastName = prompt ("Enter your last name");

		console.log("hello"+firstName+""+lastName+"!");
		console.log("welcome to my page!");

	}

	printWelcomMessage();

//naming conventions
//Function names should be definitive of the task it will perform. It usually contains a verb.


//Avoid generic names to avoid confusion within your code.

		function get(){

			let name = "Jamie";
			console.log(name);

		};

		get();

	//Avoid pointless and inappropriate function names.

		function foo(){

			console.log(25%5);

		};

		foo();

	//Name your functions in small caps. Follow camelCase when naming variables and functions.

		function displayCarInfo(){

			console.log("Brand: Toyota");
			console.log("Type: Sedan");
			console.log("Price: 1,500,000");

		}
		
		displayCarInfo();


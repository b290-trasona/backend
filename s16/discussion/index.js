//console.log("hello");

//Arithmetic Operators
let x = 1397;
let y = 7831;

let sum = x + y;
console.log("result of the addtion Operators: " + sum);

let difference = x - y;
console.log("result of the difference Operators: " + difference);

let product = x * y;
console.log("result of the product Operators: " + product);

let quotient = x / y;
console.log("result of the quotient Operators: " + quotient);

let remainder = x % y; 
console.log("result of the remainder Operators: " + remainder);

// Assignment operators
//basic Assignment operator(=)
// The assignment operator assigns the value of the **right** operand to a variable.
let assignmentNumber = 8;

//Addition assignment operator (+=)
// The addition assignment operator adds the value of the right operand to a variable and assigns the result to the variable.
assignmentNumber = assignmentNumber + 2;
console.log("result of the adddtionassignment operator:"+ assignmentNumber);

//shorthand for assignmenNumber = assignmentNumber + 2 :
assignmentNumber += 2 ;
console.log(assignmentNumber);

assignmentNumber -= 2 ;
console.log("result of the subtraction assignment operator:"+ assignmentNumber);

assignmentNumber *= 2 ;
console.log("result of the multiplication assignment operator:"+ assignmentNumber);
assignmentNumber /= 2 ;
console.log("result of the division assignment operator:"+ assignmentNumber);
assignmentNumber %= 2 ;
console.log("result of the modulo assignment operator:"+ assignmentNumber);


// multiple operator and parenthesis

/* When multiple operators are applied in a single statement, it follows the PEMDAS (Parenthesis, Exponents, Multiplication, Division, Addition and Subtraction) rule
            - The operations were done in the following order:
                1. 3 * 4 = 12
                2. 12 / 5 = 2.4
                3. 1 + 2 = 3
                4. 3 - 2.4 = 0.6
*/

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("mdas operation: " +mdas);

let pmdas = 1 + (2 - 3) * (4 / 5);
console.log("pmdas operation: " +pmdas);
/*
- By adding parentheses "()", the order of operations are changed prioritizing operations inside the parentheses first then following the MDAS rule for the remaining operations
            - The operations were done in the following order:
                1. 4 / 5 = 0.8
                2. 2 - 3 = -1
                3. -1 * 0.8 = -0.8
                4. 1 + -.08 = .2

*/

pmdas = (1 + (2 - 3)) * (4 / 5);
console.log("pmdas operation: " +pmdas);

//increment and decrement
// Operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied to
// The value of "z" is added by a value of one before returning the value and storing it in the variable "increment"
// The value of "z" was also increased even though we didn't implicitly specify any value reassignment

let z = 1;
let increment = ++z;
console.log("preincremnt: "+increment);
console.log("preincrement: "+z);
// The value of "z" is returned and stored in the variable "increment" then the value of "z" is increased by one
increment = z++;
console.log("post-increment:"+increment);
// The value of "z" is at 2 before it was incremented
console.log("post-increment:"+z);

// The value of "z" is decreased by a value of one before returning the value and storing it in the variable "decrement"
let decrement = --z;
// The value of "z" is at 3 before it was decremented
console.log("predecrement:"+decrement);
console.log("predecrement:"+z);

decrement = z--;
console.log("post-decrement:"+decrement);
console.log("post-decrement:"+z);

//comparison operators
let juan = "juan";

	// loose equality operator
/** 
            - Checks whether the operands are equal/have the same content
            - Also COMPARES the data types of 2 values
            - JavaScript is a loosely typed language meaning that values of different data types can be stored in variables
            - In combination with type coercion, this sometimes creates problems within our code (e.g. Java, Typescript)
            - Some programming languages require the developers to explicitly define the data type stored in variables to*/
	console.log(1 == 1);
	console.log(1 == 2);
	console.log(1 == "1");
	console.log(0 == false);
	console.log("juan" == "juan");
	console.log("juan" == juan);

	// strict equality operator
	/* 
            - Checks whether the operands are equal/have the same content
            - Also COMPARES the data types of 2 values
            - JavaScript is a loosely typed language meaning that values of different data types can be stored in variables
            - In combination with type coercion, this sometimes creates problems within our code (e.g. Java, Typescript)
            - Some programming languages require the developers to explicitly define the data type stored in variables to prevent this from happening
            - Strict equality operators are better to use in most cases to ensure that data types provided are correct
        */

	console.log(1 === 1);
	console.log(1 === 2);
	console.log(1 === "1");
	console.log(0 === false);
	console.log("juan" === "juan");
	console.log("juan" === juan);

	//loose inequality operator
	/*- Checks whether the operands are not equal/have the same content
            - Also COMPARES the data types of 2 values
        */
	console.log(1 != 1);
	console.log(1 != 2);
	console.log(1 != "1");
	console.log(0 != false);
	console.log("juan" != "juan");
	console.log("juan" != juan);
	//strict inequality
	console.log(1 !== 1);
	console.log(1 !== 2);
	console.log(1 !== "1");
	console.log(0 !== false);
	console.log("juan" !== "juan");
	console.log("juan" !== juan);

//relational operators
//Some comparison operators check whether one value is greater or less than to the other value.
let a = 50;
let b = 65;

//GT or greater than operator(>)
let isGreaterThan = a > b;
//LT or less than operator (<)
let isLessThan = a < b;
//GTE or greater than or Equal (>=)
let isGTorEqual = a >= b;
//LTE or greater than or Equal (<=)
let isLTorEqual = a <= b;

console.log(isGreaterThan);
console.log(isLessThan);
console.log(isGTorEqual);
console.log(isLTorEqual);

let numStr = "30";
console.log(a > numStr);//true - converted string to number
console.log(b <= numStr);//false

let str = "twenty";
console.log(b >= str);//false str resulted to NaN string is not numeric

//logical operators
	
	let isLegalAge = true;
	let isRegistered = false;

	//AND Operator(&&)
	/*
		1 * 0 = 0
		1 * 1 = 1
		0 * 1 = 0
		0 * 0 = 0

	*/
	let allRequirementsMet = isLegalAge && isRegistered;
	console.log("result of logical AND operator: "+allRequirementsMet);

	//OR operator (||) = Douple pipe/bar

	// Returns true if one of the operands are true
	let someRequirementsMet = isLegalAge || isRegistered;
	console.log("result of logical OR operator: "+someRequirementsMet);

	//NOT operator ! exclamation
	//returns opposite

	let maybeRequirementsMet = !isRegistered;
	